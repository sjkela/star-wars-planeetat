import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  planeetat: any[] = [];
  eliot: any[] = [];

  next_url = 'https://swapi.co/api/planets';

  suoritettu = false;

  elio: any = 'alkuteksti';

  constructor(private http: HttpClient) { }

  ngOnInit() {
    //this.playTheme();
    this.hae(this.next_url);
  }

  async hae(url) {
    //haetaan planeetat apista url osoitteella
    this.http.get(url).subscribe((res: any) => {
      //taulukoidaan planeetat
      const p: any[] = res.results;

      //taulukoidaan jokaisen planeetan asukkaiden url osoitteet
      p.forEach( planeetta => {
        let residentUrl: any[] = planeetta.residents;

        planeetta.eliot = [];

        //käydään läpi kaikki asukkaat
        residentUrl.forEach( asukkaat => {
          console.log('Haetaan ' + asukkaat);

          //haetaan asukkaiden lajien url osoitteet taulukkoon
          this.http.get(asukkaat).subscribe((res: any) => {
            let speciesUrl: any[] = res.species;

            //käydään lajit läpi
            speciesUrl.forEach(lajit => {
              console.log('Haetaan ' + res.name);
              this.http.get(lajit).subscribe((res: any) => {
                //jos eliölajia ei vielä löydy niin lisätään se planeettaan
                if (planeetta.eliot.find(elio => elio.name == res.name) == null) {
                  planeetta.eliot.push({name: res.name, url: lajit});
                }
              });
            })
          });
        });

        //lisätään uusi planeetta
        this.planeetat.push(planeetta)
      });

      console.log('Haetaan ' + res.next + '...');

      //haetaan uusi sivu planeettoja.
      //Muussa tapauksessa järjestetään planeetat.
      if(res.next != null) {
        this.hae(res.next);
      } else {
        this.jarjesta();
      }
    });
  }

  jarjesta() {
    this.planeetat = this.planeetat.sort((a: any, b: any) => {

      const halkA = parseInt(a.diameter);
      const halkB = parseInt(b.diameter);

      if(isNaN(halkB)) {
        return -1;
      }
      else if (isNaN(halkA)) {
        return 1;
      }
      else if (halkA < halkB) {
        return 1;
      }
      else {
        return -1;
      }

    });

    this.suoritettu = true;
  }

  ladataanElio(e) {
    this.elio = null;

    //Haetaan eliö ja eliön kotiplaneetta
    this.http.get(e).subscribe((res: any) => {
      let e = res;

      if(e.homeworld!=null) {
        this.http.get(e.homeworld).subscribe((res: any) => {
          e.koti = res.name;
          this.elio = e;
        })
      }
      //koska kaikille eliöille ei ole kotiplaneetta
      //niin käsitellään null
      else{
        e.koti = 'null';
        this.elio = e;
      }
    });
  }
}
